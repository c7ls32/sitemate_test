from flask import Flask, jsonify, request
from flask_cors import CORS
import random

app = Flask(__name__)
CORS(app)

allData=[{'id':'1000','title':'Init','description':'Init'}]
@app.route("/create", methods=['POST'])
def create():
    data = request.json
    data['id']=random.randint(1000, 9999)
    allData.append(data)    
    return jsonify(allData)

@app.route("/delete", methods=['POST'])
def delete():
    data = request.json
    for index,i in enumerate(allData):
        if i['id']==data['index']:
            allData.pop(index)
    return jsonify(allData)

@app.route("/get", methods=['POST'])
def get():
    return jsonify(allData)

@app.route("/update", methods=['POST'])
def update():
    data = request.json
    for index,i in enumerate(allData):
        if i['id']==data['id']:
            allData[index]=data
    return jsonify(allData)

if __name__ == "__main__":
    app.run(debug=True)