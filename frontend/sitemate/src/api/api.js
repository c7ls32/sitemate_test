// import axios from 'axios'
const URL = 'http://127.0.0.1:5000/';

const apiRequest = async (type, method, request) => {
    let response;
    switch (type) {
        case 'create': {
            const options = {
                method: "POST",
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            response = await fetch(URL+type,options)
            break;
        }
        case 'get': {
            const options = {
                method: "POST",
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            response = await fetch(URL+type,options)
            break;
        }
        case 'update': {
            const options = {
                method: "POST",
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            response = await fetch(URL+type,options)
            break;
        }
        case 'delete': {
            const options = {
                method: "POST",
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            response = await fetch(URL+type,options)
            break;
        }
        default:{
            console.log('error')
        }

    }
    return response;
}

export default apiRequest;
